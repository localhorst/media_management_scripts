#!/bin/bash

#####################################################
#                                                   #
#                   SCRIPT USAGE                    #
#                                                   #
#     copy script in root from hdd beside the       #
#      folderers /movies and /tvshows and run       #
#       with sh ./check_Names_and_Paths.sh          #
#                                                   #
#                                                   #
#####################################################

echo "Starting script ..."

if [[ -d movies ]]
then
    echo "Movie directory found"
else 
    echo "Movie directory not found"
    exit 1
fi

if [[ -d tvshows ]]
then
    echo "TV-Shows directory found"
else 
    echo "TV-Shows directory not found"
    exit 1
fi

if [[ -d books ]]
then
    echo "Books directory found"
else 
    echo "Books directory not found"
    exit 1
fi

echo ""
echo "Checking Movies ..."
echo ""

for file in movies/* 
do
    filename=$(basename "$file") # file name with extension
    filebasename=$(echo "$filename" | cut -f 1 -d '.') # file name without extension
    #echo "Processing $file"
    #echo "filename: $filename"
    #echo "filebasename: $filebasename"
 
    ###### check for amount of points ######
    pointCount=$(echo "$filename" | tr -cd '.' | wc -c)
    if [[ $pointCount != 1 ]]
    then
        echo "Incident: Incorrect amount of points: $file"
    fi

    ###### check extension ######
    ext="${filename##*.}"
    if  [ "$ext" != "mkv" ] && [ "$ext" != "mp4" ]
    then
        echo "Incident: Incorrect extension: $file"
    fi

    ###### check for not allowed chars ######
    if [[ ! "$filebasename" =~ ^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0-9\\-\\_]+$ ]]
    then
        echo "Incident: Not allowed char found in: $file"
    fi
done

#done with movies

echo ""
echo "Checking TV-Shows ..."
echo ""

for show in tvshows/*; do
    if [[ -d $show ]]; then
        showname=$(basename "$show") # file name with extension
        #echo "TV-Show found: $show"
        #echo "showname: $showname"
    
        ###### check for not allowed chars in showname ######
        if [[ ! "$showname" =~ ^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0-9\\-\\_]+$ ]]
        then
            echo "Incident: Not allowed char found in: $show"
        fi
    
        for season in tvshows/$showname/*; do
            if [[ -d $season ]]; then
                seasonname=$(basename "$season") # file name with extension
                #echo "Season found: $season"
                #echo "seasonname: $seasonname"
                
                ###### check for not allowed chars in season name ######
                if [[ ! "$seasonname" =~ ^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0-9\\-\\_]+$ ]]
                then
                    echo "Incident: Not allowed char found in: $season"
                fi
                
                for episode in tvshows/$showname/$seasonname/*; do
                    if [[ -f $episode ]]; then
                        episodename=$(basename "$episode") # file name with extension
                        episodebasename=$(echo "$episodename" | cut -f 1 -d '.') # file name without extension
                        #echo "Episode found: $episode"
                        #echo "Episodename: $episodename"
                        #echo "Episodebasename: $episodebasename"
                        
                        ###### check for amount of points ######
                        pointCount=$(echo "$episodename" | tr -cd '.' | wc -c)
                        if [[ $pointCount != 1 ]]
                        then
                            echo "Incident: Incorrect amount of points: $episode"
                        fi

                        ###### check extension ######
                        ext="${episodename##*.}"
                        if  [ "$ext" != "mkv" ] && [ "$ext" != "mp4" ]
                        then
                            echo "Incident: Incorrect extension: $episode"
                        fi

                        ###### check for not allowed chars ######
                        if [[ ! "$episodebasename" =~ ^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0-9\\-\\_]+$ ]]
                        then
                            echo "Incident: Not allowed char found in: $episode"
                        fi
                    fi
                done
            fi
        done
    fi
done

echo ""
echo "Checking Books ..."
echo ""

for file in books/* 
do
    filename=$(basename "$file") # file name with extension
    filebasename=$(echo "$filename" | cut -f 1 -d '.') # file name without extension
    #echo "Processing $file"
    #echo "filename: $filename"
    #echo "filebasename: $filebasename"
 
    ###### check for amount of points ######
    pointCount=$(echo "$filename" | tr -cd '.' | wc -c)
    if [[ $pointCount != 1 ]]
    then
        echo "Incident: Incorrect amount of points: $file"
    fi

    ###### check extension ######
    ext="${filename##*.}"
    if  [ "$ext" != "pdf" ] && [ "$ext" != "epub" ]
    then
        echo "Incident: Incorrect extension: $file"
    fi

    ###### check for not allowed chars ######
    if [[ ! "$filebasename" =~ ^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0-9\\-\\_]+$ ]]
    then
        echo "Incident: Not allowed char found in: $file"
    fi
done

#done with books

echo ""
echo "Finished script successfully"
