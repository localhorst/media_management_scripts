#! /bin/sh

#######################################  HOW TO USE  ################################################
#   manually set MOVIES_DIR
#   manually create and set OUTPUT_DIR
#   manually set movies names in array
#####################################################################################################

MOVIES_DIR=/home/hendrik/git/media_management_scripts/codec_visualizer/
OUTPUT_DIR=/home/hendrik/git/media_management_scripts/av1output/

movies=("test.mp4")

echo "starting script ..."
echo " "

for movie in ${movies[@]}; do
    echo "loop for $movie"

    NEW_MOVIE_NAME=$"$(b=${movie##*/}; echo ${b%.*}).mkv"
    TMP_MOVIE_NAME=$"tmp_$(b=${movie##*/}; echo ${b%.*}).mkv"
    SCREENSHOT_MOVIE_NAME=$"tmp_$(b=${movie##*/}; echo ${b%.*}).png"
   
    echo "NEW_MOVIE_NAME: $NEW_MOVIE_NAME"
    echo "TMP_MOVIE_NAME: $TMP_MOVIE_NAME"

    if [ -f "$OUTPUT_DIR$TMP_MOVIE_NAME" ]; then
        echo "warning: tmp file exists --> delete $TMP_MOVIE_NAME"
        rm "$OUTPUT_DIR$TMP_MOVIE_NAME" 
    fi

    if [ -f "$OUTPUT_DIR$NEW_MOVIE_NAME" ]; then
        echo "$NEW_MOVIE_NAME exists."
    else
        #ffmpeg -i $MOVIES_DIR$movie -c:v libaom-av1 -c:a libopus -mapping_family 1 -af aformat=channel_layouts=5.1 -c:s copy -map 0 -crf 24 -b:v 0 -b:a 128k -cpu-used 4 -row-mt 1 -tiles 2x2 $OUTPUT_DIR$TMP_MOVIE_NAME
        ffmpeg -i $MOVIES_DIR$movie -vcodec libx264 -acodec aac $OUTPUT_DIR$TMP_MOVIE_NAME
        mv $OUTPUT_DIR$TMP_MOVIE_NAME $OUTPUT_DIR$NEW_MOVIE_NAME
        echo "$NEW_MOVIE_NAME converted."

        codecVis $MOVIES_DIR$movie $OUTPUT_DIR$NEW_MOVIE_NAME
        mv output.png $OUTPUT_DIR$SCREENSHOT_MOVIE_NAME
    fi
done