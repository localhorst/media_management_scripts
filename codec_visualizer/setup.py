from setuptools import setup

setup(
    name="codecVis",
    version="0.0.1",
    packages=["codecVis"],
    entry_points={
        "console_scripts": [
            "codecVis = codecVis.__main__:main"
        ]
    },
    install_requires=[
        'Pillow>=9.2.0'
    ]
)