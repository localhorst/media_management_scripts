from setuptools import setup

setup(
    name="checkMetadata",
    version="0.0.1",
    packages=["checkMetadata"],
    entry_points={
        "console_scripts": [
            "checkMetadata = checkMetadata.__main__:main"
        ]
    },
)