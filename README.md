# codec_visualizer

## Usage 

`python ./codec_analyzer.py file file`

## Features
- mutiple resolutions
- all codecs from ffmpeg
- zoomed in subframe 

## Requirements
- ffmpeg

## Demo

![alt text](https://git.mosad.xyz/localhorst/media_management_scripts/raw/commit/e67c26a2bcbc6be4674bd0e852254d2afb481ea5/codec_visualizer/demo_output.png "demo output image")


# check_metadata

## Usage 

`python ./check_metadata.py path`

## Features
- find all video files in path
- list following metadata:
- - Name
- - Duration
- - Filesize
- - codec 
- - resolution

## Requirements
- ffmpeg
- pip3 install tqdm

## Demo
```
The_Movie_01.mkv                                                | 2:18:21  | 1.57 GiB         | h264     | 1920x1080       
The_Movie_02.mkv                                                | 1:52:11  | 1.92 GiB         | h264     | 1916x798        
The_Movie_03.mkv                                                | 2:23:11  | 1.05 GiB         | av1      | 1920x804        
...        
The_Movie_04.mkv                                                | 2:18:24  | 1.21 GiB         | h264     | 720x560         

h264     | 3 files  | 87.7%   
hevc     | 8 files  | 6.3%    
mpeg4    | 11 files | 2.7%    
av1      | 10 files | 2.4%    
vp9      | 4 files  | 1.0%    

total 36 files
```
# convert 

TODO
